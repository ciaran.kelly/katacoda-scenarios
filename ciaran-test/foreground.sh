#!/bin/bash

JXVERSION=$(curl --silent "https://github.com/jenkins-x/jx/releases/latest" | sed 's#.*tag/\(.*\)\".*#\1#')
JXURL="https://github.com/jenkins-x/jx/releases/download/${JXVERSION}/jx-linux-amd64.tar.gz"

echo "Fetching JX (${JXVERSION})"
curlbar -LO $JXURL
tar xf jx-linux-amd64.tar.gz
sudo mv jx /usr/local/bin